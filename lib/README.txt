Any lib that gets compiled by the project, third party or any needed
in development. Prior to deployment, third party libraries get moved
to /usr/local/lib, where they belong, leaving the project clean.
