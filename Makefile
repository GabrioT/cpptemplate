CC := g++
SRCDIR := src
BUILDDIR := build
TARGET := bin/main

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -g -Wall
LIB := -pthread -L lib
INC := -I include

$(TARGET): $(OBJECTS)
	@echo "Linking..."
	$(CC) $^ -o $(TARGET) $(LIB)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@mkdir -p $(BUILDDIR)
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

clean:
	@echo "Cleaning..."
	$(RM) -r $(BUILDDIR)/* $(TARGET)

test:
	@echo "Building tests..."
	$(CC) $(CFLAGS) test/tester.cpp $(INC) $(LIB) -o bin/ticket;

spike:
	@echo "Building spikes..."
	$(CC) $(CFLAGS) spikes/spike.cpp $(INC) $(LIB) -o bin/spike
